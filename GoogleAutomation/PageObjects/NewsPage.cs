﻿using GoogleAutomation.Common;
using GoogleAutomation.UIActions;

namespace GoogleAutomation.PageObjects
{
    class NewsPage :BaseSteps
    {
        public NewsPageAction GoTo()
        {
            return new NewsPageAction();
        }
    }
}
