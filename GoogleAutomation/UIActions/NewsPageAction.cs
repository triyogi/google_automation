﻿using GoogleAutomation.Common;
using GoogleAutomation.Selenium;
using OpenQA.Selenium;
using System.Drawing;

namespace GoogleAutomation.UIActions
{
    public class NewsPageAction:SetUp
    {
        string partialUrl;
        Configuration config = new Configuration();

        public string SelectCountry(string country)
        {
            if (country == "uk")
            {
                partialUrl = "?ned=" + country + "&hl=en-GB";
            }
            else
            {
                partialUrl = "?ned=" + country + "&hl=en-GB";
            }
            return partialUrl;
        }

        public void NavigateTo(string country)
        {
            driver.Navigate().GoToUrl(config.UrlValue+ SelectCountry(country));
        }

        public string CountryText()
        {
            return driver.FindElement(By.XPath("//*[@id='gb']/div[4]/div[2]/div/c-wiz/div/div/div/div/a[3]/content/div[2]/div/span[2]")).Text;
        }

        int a;
        int b;
        int c;

        public string HeadlinesOpen()
        {
            string color = driver.FindElement(By.XPath("//*[@id='gb']/div[3]/div/c-wiz/div/div[1]/div[1]/div/div[1]/content/span")).GetCssValue("color");
            string[] colors=color.Split(' ','(', ')',',');
            

            for(int i=0;i<8; i++)
            {
                if (i == 3)
                {
                    b = int.Parse(colors[i]);
                }
                else if(i == 5)
                {
                    c = int.Parse(colors[i]);
                }
                else if(i == 1)
                {
                    a = int.Parse(colors[i]);
                }                
            }

            string hexColor = string.Format("0x{0:X8}", Color.FromArgb(a, b, c).ToArgb());

            return hexColor.ToLower();
        }

        public bool VerifyImagePresent()
        {
            bool result=false;
            for(int i = 1; i <= 6; i++)
            {
                var e = driver.FindElement(By.XPath("//*[@id='yDmH0d']/c-wiz/c-wiz/main/div[1]/div[1]/c-wiz/div/c-wiz[" + i + "]/c-wiz/div/div[1]/a/img")).GetAttribute("src");

                if (e.Length != 0)
                {
                    result = true;
                    if (result == false)
                    {
                        break;
                    }
                    
                }                
            }
            return result;
        }
    }
}
