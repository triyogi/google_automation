﻿using GoogleAutomation.PageObjects;
using GoogleAutomation.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace GoogleAutomation.AcceptanceTests
{
    [Binding]
    public class GoogleHeadlinesSteps : SetUp
    {
        NewsPage newsPage = new NewsPage();

        [Given(@"I am on google home page")]
        public void GivenIAmOnGoogleHomePage()
        {
            newsPage.NavigateTo();
        }
        
        [When(@"I select '(.*)' to read news")]
        public void WhenISelectToReadNews(string country)
        {
            newsPage.GoTo().NavigateTo(country);
        }
        
        [Then(@"News headlines should display")]
        public void ThenNewsHeadlinesShouldDisplay()
        {
            Assert.IsTrue(newsPage.GoTo().HeadlinesOpen().Contains("4285f4"));
        }
        
        [Then(@"Each news headline should contain an image")]
        public void ThenEachNewsHeadlineShouldContainAnImage()
        {
           Assert.IsTrue(newsPage.GoTo().VerifyImagePresent());
        }
        
        [Then(@"'(.*)' should be displayed under the Sections")]
        public void ThenShouldBeDisplayedUnderTheSections(string country)
        {
            Assert.AreEqual(newsPage.GoTo().CountryText(), country);
        }
    }
}
