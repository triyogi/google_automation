﻿Feature: GoogleHeadlines
         As a google user
	     I want to read uk news headlines

  Background: 
		Given I am on google home page	    

  @PicNextToNews
  Scenario: Verify there is a picture next to every news story
        When I select 'uk' to read news
	    Then News headlines should display
		And Each news headline should contain an image 

  @CountryPresent
  Scenario: In the sections area, there is always U.K. present
        When I select 'uk' to read news
	    Then 'U.K.' should be displayed under the Sections
		
		
