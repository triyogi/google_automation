﻿
using GoogleAutomation.Selenium;

namespace GoogleAutomation.Common
{
    public abstract class BaseSteps : SetUp
    {
        Configuration config = new Configuration();

        public void NavigateTo()
        {
            driver.Navigate().GoToUrl(config.UrlValue);
        }
    }
}
